﻿using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class EmptyEntity
	{
		protected internal EmptyEntity() { }
	}
}