using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class KeyedEntity<TKey> : EmptyEntity where TKey : struct
	{
		public TKey Id { get; set; }

		protected internal KeyedEntity() { }
	}
}