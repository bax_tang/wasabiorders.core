using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class ObjectEntity<TKey> : KeyedEntity<TKey> where TKey : struct
	{
		public DateTime CreatedAt { get; set; }

		protected internal ObjectEntity() : base() { }
	}
}