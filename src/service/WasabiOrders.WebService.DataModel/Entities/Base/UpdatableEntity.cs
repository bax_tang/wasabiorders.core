using System;

namespace WasabiOrders.WebService.DataModel.Entities
{
	public abstract class UpdatableEntity<TKey> : ObjectEntity<TKey> where TKey : struct
	{
		public DateTime? UpdatedAt { get; set; }

		protected internal UpdatableEntity() : base() { }
	}
}